## What I've tried:

I refactored the transition code somewhat, by flattening the data structure. 

I've also simplified my goals, because I think I was making things unnecessarily complicated. 
I'm no longer aiming to transition **any** component of any size to any other, but rather each child page _always_ expands out of and contracts back to the same component on its parent page, i.e. **PageA** will always grow from and shrink back to **OriginA** on PageHome - never OriginB, or an origin on a different page. The size of child and parent pages will also _always_ be 100vw × 100vh, to make things simpler.

My first approach was to make the `expand` transition into its own component, _TransitionExpand.vue_, and I tried applying it around:

- `<router-view>` in _App.vue_

- `<router-link :to="{ name: 'A'}>` in _PageHome.vue_

- `<div class="page">` in _PageA.vue_

None of these attempts worked, the common issue was that the click event was emitted, but it was not being caught by `@originClick="setClickOrigin"`

I then followed Markus Oberlehner's post on [vue-router page transitions](https://markus.oberlehner.net/blog/vue-router-page-transitions/) while trying to adapt it to my own purposes (i.e. using javascript rather than CSS transitions).

The plan was to have two different `enter` methods, `expandEnter` and `contractEnter`. 
The transition name and mode would be dynamically bound, and I tried to do something similar with the `enter` method:

```vue
<transition
  :name="transitionName"
  :mode="transitionMode"
  @enter="transitionEnter"
  :css="false"
>
  <router-view @originClick="setClickOrigin"/>
</transition>
    
```

_(note: `transitionMode` is whether the new element animates in before the old animates out, or whether the old element animates out and then the new animates in)_

Some defaults are declared: 

```javascript
const DEFAULT_TRANSITION = 'expand'
const DEFAULT_TRANSITION_MODE = 'in-out'
const DEFAULT_TRANSITION_ENTER = 'expandEnter'
const DEFAULT_TRANSITION_ORIGIN = '#a'
export default {
  data () {
    return {
      transitionName: DEFAULT_TRANSITION,
      transitionMode: DEFAULT_TRANSITION_MODE,
      transitionEnter: DEFAULT_TRANSITION_ENTER,
      transitionOrigin: DEFAULT_TRANSITION_ORIGIN,
      ...
```

A `created` hook will determine which transition to use, based on the route's meta property.

```javascript 
  created () {
    this.$router.beforeEach((to, from, next) => {
      const transitionName = to.meta.transitionName || DEFAULT_TRANSITION
      const transitionMode = to.meta.transitionMode || DEFAULT_TRANSITION_MODE
      const transitionOrigin = from.meta.transitionOrigin || to.meta.transitionOrigin || DEFAULT_TRANSITION_ORIGIN
      const subPage = from.meta.transitionOrigin || to.meta.transitionOrigin || DEFAULT_TRANSITION_ORIGIN

      this.transitionName = transitionName
      this.transitionMode = transitionMode
      this.transitionOrigin = transitionOrigin

      next()
    })
  },
```
and then within the `enter` method, the animation properties could be determined with an `if` statement.

```javascript
    enter (element, done) {
      if (this.transitionName === 'expand') {
        // calculate start and end properties, 
        // then store them in this.animateFrom and this.animateTo
      }
      if (this.transitionName === 'contract') {
        // calculate start and end properties, 
        // then store them in this.animateFrom and this.animateTo
      }
      
      // main animation loop
      const tick = () => {
      	// get animation duration, current progress and the animateFrom & animateTo values
      	// calculate current transform value
        // apply transform values to element
        if (progress === 1) {
          return
        }
        requestAnimationFrame(tick)    
      }
      tick()
    }
      
```

This 'worked', except that of course I was applying the animation to the element that was _entering_.  
That's fine for my original case (**expanding** a child page from a small 'origin box' on its parent page until it is full-screen). It's not what I want for the second case - **contracting** the child page back down to the small origin box on the parent page. In this case, the parent page should already be visible and the child page that we're transitioning away from is the animated element. I therefore need to put my `contract` transition in the `leave` animation hook, not the `enter` hook.

One big ugly copy & paste later...

```vue
<transition
  :name="transitionName"
  :mode="transitionMode"
  @leave="leave"
  @enter="enter"
  :css="false"
>
```

```javascript
    leave (element, done) {
      console.log(element.id, 'leaving')
      
      if (this.transitionName === 'expand') {
        return done
      }
      
      if (this.transitionName === 'contract') {
        // calculate start and end properties, 
        // then store them in this.animateFrom and this.animateTo
      }

      // main animation loop
      const tick = () => {
        // get animation duration, current progress and the animateFrom & animateTo values
        // calculate current transform value
        // apply transform values to element
        if (progress === 1) {
          return
        }
        requestAnimationFrame(tick)    
      }
      tick()
    },

    enter (element, done) {
      console.log(element.id, 'entering')
      
      if (this.transitionName === 'contract') {
        return done
      }
      
      if (this.transitionName === 'expand') {
        // calculate start and end properties, 
        // then store them in this.animateFrom and this.animateTo
      }

      // main animation loop
      const tick = () => {
        // get animation duration, current progress and the animateFrom & animateTo values
        // calculate current transform value
        // apply transform values to element
        if (progress === 1) {
          return
        }
        requestAnimationFrame(tick)    
      }
      tick()
    }
```

_Now,_ the child page contracts down to the dimensions of the origin box - hooray!  
...but so does the parent page. The whole website is scrunched into a tiny box. 

Checking what I logged to the console, I see:

```
child entering!          App.vue:152
parent leaving!          App.vue:93
child leaving!           App.vue:93
```
So I need to figure out how to stop the parent leaving. Can I use one of the other [Vue transition hooks](https://vuejs.org/v2/guide/transitions.html#JavaScript-Hooks) to do so, or is there something I can fix in my existing `leave`/ `enter` methods?

I'm pretty sure that _if_ I can get this to work, I don't even need to emit a click event, or use an event bus - I can instead set the origin box for each child page in the `meta` property of its route, and add that info to `data` in the `created()` hook.