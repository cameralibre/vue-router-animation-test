import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/PageHome.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { transitionName: 'slide' }
  },
  {
    path: '/right',
    name: 'Right',
    component: () => import(/* webpackChunkName: "animation" */ '../views/PageRight.vue'),
    meta: { transitionName: 'slide', direction: 'right' }
  },
  {
    path: '/left',
    name: 'Left',
    component: () => import(/* webpackChunkName: "animation" */ '../views/PageLeft.vue'),
    meta: { transitionName: 'slide', direction: 'left' }
  },
  {
    path: '/left-a',
    name: 'LeftA',
    component: () => import(/* webpackChunkName: "animation" */ '../views/PageLeftA.vue'),
    meta: { transitionName: 'expand', origin: '#a' }
  },
  {
    path: '/left-b',
    name: 'LeftB',
    component: () => import(/* webpackChunkName: "animation" */ '../views/PageLeftB.vue'),
    meta: { transitionName: 'expand', origin: '#b' }
  },
  {
    path: '/left-c',
    name: 'LeftC',
    component: () => import(/* webpackChunkName: "animation" */ '../views/PageLeftC.vue'),
    meta: { transitionName: 'expand', origin: '#c' }
  },
  {
    path: '/right-a',
    name: 'RightA',
    component: () => import(/* webpackChunkName: "animation" */ '../views/PageRightA.vue'),
    meta: { transitionName: 'expand', origin: '#a' }
  },
  {
    path: '/right-b',
    name: 'RightB',
    component: () => import(/* webpackChunkName: "animation" */ '../views/PageRightB.vue'),
    meta: { transitionName: 'expand', origin: '#b' }
  },
  {
    path: '/right-c',
    name: 'RightC',
    component: () => import(/* webpackChunkName: "animation" */ '../views/PageRightC.vue'),
    meta: { transitionName: 'expand', origin: '#c' }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
